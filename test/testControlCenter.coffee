ControlCenter = require('../lib/ControlCenter')
chai = require 'chai'
should = chai.should()
chaiAsPromised = require 'chai-as-promised'
chai.use(chaiAsPromised)
Lib = require './lib/lib.coffee'

dbURI = 'mongodb://localhost/test-ne-webportal'

mongoose = require 'mongoose'
Patient = require '../models/PatientModel'
#clearDB = require('mocha-mongoose')(dbURI)

clearPatientsFromMongo = (done)->
  unless mongoose.connection.db
    mongoose.connect dbURI
    Patient.remove({}, (err) ->
      console.log "in the callback"
      if err
        console.log err
      done()
    )

describe 'contructor', () ->
  it 'control center should be an object', () ->
    ControlCenter.should.be.a('object')

describe 'adding patients' , () ->
  before (done) ->
    Lib.clearPatientsFromMongo done
  
  it 'number of patients in db should be 0', () ->
    Patient.find().count (err, result) ->
      should.not.exist err
      result.should.equal 0

  it 'addPatient should be a function' , () ->
    ControlCenter.addPatient.should.be.a 'function'
  
  it 'adding a patient should trigger the callback', (done) ->
    options =
      phone: 3035961541
      conditions:
        condition1: true
        condition2: true
        condition3: false

    ControlCenter.addPatient(options, (err) ->
      should.not.exist err
      done()
    )
  
  it 'number of patients in db should now be 1', (done) ->
    Patient.find().count (err, result) ->
      should.not.exist err
      result.should.equal 1
      done()
    
describe 'uploading csv', () ->
  before (done) ->
    clearPatientsFromMongo done

  it 'loadDataToMongo should be a function', ()->
    ControlCenter.loadDataToMongo.should.be.a 'function'

  it 'loadDataToMongo should error if file is not a .csv', ()->
    ControlCenter.loadDataToMongo 'test.txt', (err)->
      should.exist err

  it 'should not error if file is a .csv' , ()->
    ControlCenter.loadDataToMongo 'test.csv', (err)->
      should.exist err

  it 'should have filled the mongo with the number of patients in all the test files', (done)->
    clearPatientsFromMongo done


describe 'filtering patients', () ->
  before (done) ->
    clearPatientsFromMongo done
  
  it 'filterPatients should be a ControlCenter function', ()->
    ControlCenter.filterPatients.should.be.a 'function'

  it 'should return all patients when given null filter', (done)->
    should.not.exist ControlCenter.filterPatients null


