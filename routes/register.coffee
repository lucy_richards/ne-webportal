express = require('express')
router = express.Router()
NurseEducator = require '../models/NurseEducatorModel'

## GET registation page 
#router.get( '/', (req, res) ->
  #info = req.flash 'info'
  #success = req.flash 'success'
  #warning = req.flash 'warning'
  #error = req.flash 'error'
  
  #res.render 'register', {\
    #error: error
    #info: info
    #warning:warning
    #success: success
    #authenticating: true
  #}
#)

router.post '/' , (req, res) ->
  console.log 'about to register a nurse educator, ' , req.body
  console.log 'req.files' , req.files
   
  photo = if req.files and req.files.photo then req.files.photo.name

  ne = new NurseEducator
    email: req.body.email
    name: req.body.name
    photo: photo
    indianState: req.body.indianState

  NurseEducator.register( ne, req.body.password, (err, account) ->
    if err
      res.render 'login',  {error: err.message, register: true}
    else
      req.logIn account, (err) ->
        if err
          next err
        else
          res.redirect '/'
      )
module.exports = router
