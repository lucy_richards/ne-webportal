express = require('express')
router = express.Router()
passport = require 'passport'

# GET login page. 
router.get '/', (req, res) ->
  console.log "serving the login page"
  info = req.flash 'info'
  success = req.flash 'success'
  warning = req.flash 'warning'
  error = req.flash 'error'
  
  res.render 'login', {\
    error: error
    info: info
    warning:warning
    success: success
    register: false
    authenticating: true
  }

router.post '/' , passport.authenticate('local', \
    {successRedirect: '/', failureRedirect: '/login', \
      failureFlash:true, successFlash: "Welcome back!"})
  


module.exports = router
