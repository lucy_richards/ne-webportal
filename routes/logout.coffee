express = require('express')
router = express.Router()

# GET home page. 
router.get('/', (req, res) ->
  req.logout()
  res.redirect '/'
)


module.exports = router
