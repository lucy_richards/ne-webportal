express = require('express')
router = express.Router()

# GET home page. 
router.get('/', (req, res) ->
  info = req.flash 'info'
  success = req.flash 'success'
  warning = req.flash 'warning'
  error = req.flash 'error'

  res.render 'index', {\
    title: 'Nurse Educator Web Portal'
    user: req.user
    error: error
    info: info
    success: success
    warning: warning
  }
)

module.exports = router
