express = require('express')
router = express.Router()
Patient = require '../models/PatientModel'
ControlCenter = require '../lib/ControlCenter'

#get active patients from db
router.get '/active', (req, res) ->
  if !req.user
    req.flash 'error', "You're not logged in! Please log in."
    req.redirect '/'
    return
  
  console.log "Getting all active patients"
  Patient.find({tags: {$not: {$in: ["nonactive"]}}}, (err, results) ->
    if err
      console.log "there was an error retrieving active patients"
      req.flash 'error', err
      res.redirect '/'
    else
      console.log " Active Patients retrieved! sending..."
      res.send results
  )

#get patients from db
router.get '/all', (req, res) ->
  if !req.user
    req.flash 'error', "You're not logged in! Please log in."
    req.redirect '/'
    return
  
  console.log "Getting all patients"
  Patient.find({}, (err, results) ->
    if err
      console.log "there was an error retrieving patients"
      req.flash 'error', err
      res.redirect '/'
    else
      console.log "Patients retrieved! sending..."
      res.send results
  )
  
# POST a new patient  
router.post '/', (req, res, next) ->
  patient = new Patient( JSON.parse req.body.patient)
  console.log patient
  patient.save( (err) ->
    if err
      console.log "There was an error retrieving patients"
      console.log err
      req.flash('error', err)
      next err
    io = app.get 'io'
    io.emit 'new patients',patient
    res.redirect '/'
  )

router.post '/uploadCSV', (req, res) ->
  console.log "in the upload CSV"
  req.flash 'success', 'Your file has been uploaded!'
  res.redirect '/'


module.exports = router
