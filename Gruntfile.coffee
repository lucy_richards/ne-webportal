module.exports = (grunt) ->
  #Project config
  grunt.initConfig
    pkg: grunt.file.readJSON 'package.json'

    clean: ['public/javascript/']
    
    wiredep:
      task:
        src: [
          'views/layouts/layout.hbs'
        ]
        options:
          directory: 'bower_components/'
          exclude: ['bower_components/bootstrap/dist/css/*']

    coffee:
      compile:
        files:
          "public/javascript/addPatientsForm.js":"public/javascript/src/addPatientsForm.coffee"
          "public/javascript/socket.js":"public/javascript/src/socket.coffee"
          "public/javascript/Patients.js":"public/javascript/src/Patients.coffee"
          "public/javascript/test/testPatients.js":"public/javascript/test/testPatients.coffee"

  #Tasks
  grunt.loadNpmTasks 'grunt-wiredep'
  grunt.loadNpmTasks 'grunt-contrib-coffee'

  #Register tasks
  grunt.registerTask 'default' , ['wiredep', 'coffee']
  grunt.registerTask 'compile' , ['coffee']

