should = chai.should

describe.only 'Patients class', ()->
  describe 'Patients.conditionList', ()->
    it 'should be a function', ()->
      Patients.conditionList.should.be.a 'function'

    it 'should return an empty string when no conditions', ()->
      patient =
        phone: '303'
        entryDate: '2222'
        conditions: []

      Patients.conditionList(patient).should.equal ''

    it "should return '1 2 3'", ()->
      patient =
        phone: '303'
        entryDate: '2222'
        conditions: ['1','2','3']

      Patients.conditionList(patient).should.equal '1 2 3'
      
    it "should return 'This should be a conditions list'", ()->
      patient =
        phone: '303'
        entryDate: '2222'
        conditions: ['this','should','be','a','conditions','list']

      Patients.conditionList(patient).should.equal 'This should be a conditions list'

    it "should return emptry string for undefined conditions", ()->
      patient =
        phone: '303'
        entryDate: '2222'
        conditions: undefined

      Patients.conditionList(patient).should.equal ''

    it "should handle strange punctuation in conditions", ()->
      patient =
        phone: '303i-596-1541'
        entryDate: '2222/333/44444'
        conditions: 'this,.should,b.e,a,wierd!conditions,list?'

      Patients.conditionList(patient).should.equal 'This .should b.e a wierd!conditions list?'
