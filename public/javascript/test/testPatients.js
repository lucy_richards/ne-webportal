(function() {
  var should;

  should = chai.should;

  describe.only('Patients class', function() {
    return describe('Patients.conditionList', function() {
      it('should be a function', function() {
        return Patients.conditionList.should.be.a('function');
      });
      it('should return an empty string when no conditions', function() {
        var patient;
        patient = {
          phone: '303',
          entryDate: '2222',
          conditions: []
        };
        return Patients.conditionList(patient).should.equal('');
      });
      it("should return '1 2 3'", function() {
        var patient;
        patient = {
          phone: '303',
          entryDate: '2222',
          conditions: ['1', '2', '3']
        };
        return Patients.conditionList(patient).should.equal('1 2 3');
      });
      it("should return 'This should be a conditions list'", function() {
        var patient;
        patient = {
          phone: '303',
          entryDate: '2222',
          conditions: ['this', 'should', 'be', 'a', 'conditions', 'list']
        };
        return Patients.conditionList(patient).should.equal('This should be a conditions list');
      });
      it("should return emptry string for undefined conditions", function() {
        var patient;
        patient = {
          phone: '303',
          entryDate: '2222',
          conditions: void 0
        };
        return Patients.conditionList(patient).should.equal('');
      });
      return it("should handle strange punctuation in conditions", function() {
        var patient;
        patient = {
          phone: '303i-596-1541',
          entryDate: '2222/333/44444',
          conditions: 'this,.should,b.e,a,wierd!conditions,list?'
        };
        return Patients.conditionList(patient).should.equal('This .should b.e a wierd!conditions list?');
      });
    });
  });

}).call(this);
