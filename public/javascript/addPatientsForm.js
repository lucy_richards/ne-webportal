(function() {
  var clearForm, insertRow;

  $(function() {
    var index;
    Patients.getActivePatients();
    index = 0;
    insertRow(index);
    $("#addPatient").click(function(e, t) {
      e.preventDefault;
      console.log("in the onclick" + index);
      index++;
      return insertRow(index);
    });
    return $("#submit_patients").click(function(e, t) {
      var nurse, result, rows, table;
      result = [];
      table = $("#table");
      rows = table.find("tr").slice(1);
      nurse = $("#nurse").val();
      result = {};
      rows.each(function(index, row) {
        var conditions, patient;
        conditions = [];
        $(this).find("input:checked").each(function(index, thing) {
          return conditions.push($(this).val());
        });
        patient = {
          nurse: nurse,
          phone: $(this).find("input[name^=phone]").val(),
          entryDate: $(this).find("input[name^=date]").val(),
          conditions: conditions
        };
        return socket.emit('save patient', patient);
      });
      return clearForm();
    });
  });

  insertRow = function(index) {
    var date, row;
    console.log("about to append row " + index);
    row = "<tr class=\"row\" id=\"" + index + "\">\n  <td>\n    <div class=\"form-group\">\n      <input type=\"tel\" class=\"form-control\" name='phone[]' placeholder=\"Phone Number\">\n    </div>\n  </td>\n  \n  <td>\n    <div class=\"form-group\">\n      <input type=\"text\" id=\"datepicker" + index + "\" class=\"form-control\" name='date[]' placeholder=\"Entry date\">\n    </div>\n  </td>\n  \n  <td >\n  <div class=\"checkbox-inline\">\n    <label>\n      <input type=\"checkbox\"  name='condition1[]' value=\"condition3\">\n      Condition 1\n    </label>\n  </div>\n  <div class=\"checkbox-inline\">\n    <label ><input type=\"checkbox\" name='condition2[]' value=\"condition1\">\n      Condition 2\n    </label>\n  </div>\n  <div class=\"checkbox-inline\">\n    <label ><input type=\"checkbox\"  name='condition3[]' value=\"condition2\">\n      Condition 3\n    </label>\n  </div>\n  </td>\n</tr>";
    $("#table").append(row);
    date = $("#datepicker" + index);
    console.log(date);
    date.datepicker();
    return date.datepicker("setDate", new Date());
  };

  clearForm = function() {
    $("#add_patients_form").trigger("reset");
    return $("#add_patients_form").find("tr:gt(1)").remove();
  };

}).call(this);
