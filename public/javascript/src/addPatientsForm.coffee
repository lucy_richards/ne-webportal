
$ ->
  Patients.getActivePatients()
  index = 0
  insertRow index

  $("#addPatient").click (e, t) ->
    e.preventDefault
    console.log "in the onclick" + index
    index++
    insertRow index

  $("#submit_patients").click (e, t) ->
    result = []
    table = $("#table")
    rows = table.find("tr")[1..]
    nurse = $("#nurse").val()
    result = {}

    rows.each (index, row) ->
      
      conditions = []
      $(this).find("input:checked").each (index, thing) ->
        conditions.push $(this).val()

      patient = {
        nurse: nurse
        phone: $(this).find("input[name^=phone]").val()
        entryDate: $(this).find("input[name^=date]").val()
        conditions: conditions
      }

      socket.emit 'save patient', patient

    clearForm()

insertRow = (index) ->
  console.log "about to append row #{index}"
  row = """
        <tr class="row" id="#{index}">
          <td>
            <div class="form-group">
              <input type="tel" class="form-control" name='phone[]' placeholder="Phone Number">
            </div>
          </td>
          
          <td>
            <div class="form-group">
              <input type="text" id="datepicker#{index}" class="form-control" name='date[]' placeholder="Entry date">
            </div>
          </td>
          
          <td >
          <div class="checkbox-inline">
            <label>
              <input type="checkbox"  name='condition1[]' value="condition3">
              Condition 1
            </label>
          </div>
          <div class="checkbox-inline">
            <label ><input type="checkbox" name='condition2[]' value="condition1">
              Condition 2
            </label>
          </div>
          <div class="checkbox-inline">
            <label ><input type="checkbox"  name='condition3[]' value="condition2">
              Condition 3
            </label>
          </div>
          </td>
        </tr>
        """
  $("#table").append row
  date = $("#datepicker#{index}")
  console.log date
  date.datepicker()
  date.datepicker("setDate", new Date())
    

clearForm = () ->
  $("#add_patients_form").trigger("reset")
  $("#add_patients_form").find("tr:gt(1)").remove()
