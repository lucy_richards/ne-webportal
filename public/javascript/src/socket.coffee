#global socket
#
window.socket = io()
socket.on 'new patient', (patient) ->
  console.log 'New patient!'
  Patients.add patient
  Patients.addToDOM patient

socket.on 'patient nonactive', (patientID) ->
  console.log 'Removing patient from the DOM'
  Patients.remove patientID
