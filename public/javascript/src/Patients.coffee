conditions = ["condition1", "condition2", "condition3"]

class Patients
  
  patients = []

  constructor: ()->

  conditionList: (patient) ->
    conditions = patient.conditions
    if conditions and conditions.length > 0
      conditions.reduce (cond1, cond2) -> "#{cond1}, #{cond2}"
    else
      return ""
  
  filter: ()->


  remove: (patientID) ->
    newPatients = patients.filter (patient)->
      return patient._id != patientID
    
    patients = newPatients
    $("##{patientID}").closest("tr").remove()

  clearPatientList: ()->
    $("#active-patients-list").empty()

  addToDOM: (patient) ->
    console.log "adding the patient to the dom"
    list = $("#active-patients-list")
    conditionList = this.conditionList patient
    phone = patient.phone
    date = patient.entryDate
    id = patient._id
    list.append("""
      <tr> 
        <td> Phone: #{phone} </td>
        <td> Entry Date: #{date} </td>
        <td> #{conditionList}</td>
        <td><button type="button"  aria-label="Close" id="#{id}"><span aria-hidden="true">&times;</span></button></td>
      </tr>
      """
    )
    
    this.appendRemovePatientHandler($("##{id}"))

  add : (patient) ->
    patients.push patient
  
  appendRemovePatientHandler : (el)->
    console.log "appending click handler"
    el.on("click", ()->
      patientId = el.closest('tr').attr 'id'
      socket.emit 'patient nonactive', el.attr 'id'
    )
    
  getAllPatients : (callback) ->
    that = this
    promise = $.ajax
      url: '/patients/all'
      success: ( patients ) ->
        console.log "success retrieving patients"
        for patient in patients
          that.add patient
          that.addToDOM patient

    promise.fail (err) ->
      alert "there was an error retrieving patients"
      console.log err
      console.log "make this into an alert banner"
  
  getActivePatients : (callback) ->
    that = this
    promise = $.ajax
      url: '/patients/active'
      success: ( patients ) ->
        console.log "success retrieving active patients"
        patients.forEach (patient) ->
          that.add patient
          that.addToDOM patient

    promise.fail (err) ->
      alert "there was an error retrieving active patients"
      console.log err
      console.log "make this into an alert banner"

window.Patients = new Patients()

