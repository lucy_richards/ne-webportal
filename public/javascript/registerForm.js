$(function () {

  $("#registerbtn").prop("disabled", true);
  
  var name, state, email, password, confirm = false;

  var loginPassword = $('#login').find('input[name=password]');
  var registerEmail = $('#login').find('input[name=password]');

  var registerPassword = $('#register').find('input[name=password]');
  var registerEmail = $('#register').find('input[name=email]');
  var indianState = $('#register').find('select[name=indianState]');
  var confirmPassword = $('#register').find('input[name=confirm]');
  var registerName = $('#register').find('input[name=name]');
  
  $("#login-tab").on('click', function() {
    $("#login-form").trigger("reset");
  });

  $("#register-tab").on('click', function() {
    $("#register-form").trigger("reset");
    $(".form-group").each(function () {
      $(this).removeClass("has-success has-error has-warning");
      $(this).addClass( "has-error");
    });
  });

  //Disable form submit buttons until all fields have been completed correctly
  registerPassword.on('input', function(e, t) {
    console.log('register password inputd');
    inputPasswordFieldStates();
    checkToEnableButton();
  });
  
  confirmPassword.on('input', function(e, t) {
    console.log('confirm password inputd');
    inputPasswordFieldStates();
    checkToEnableButton();
  });
  
  indianState.on('input', function(e, t) {
    var remove, add = "";
    if(indianState.val() != ""){
      remove = 'has-error has-warning';
      add = 'has-success'; 
      state = true;
    }
    else {
      remove = 'has-success has-warning';
      add = 'has-error';
      state = false;
    }
    changeInputState(indianState, remove, add);
    checkToEnableButton();
  });
  registerName.on('input', function(e, t) {
    var remove, add = "";
    if(registerName.val() != ""){
      remove = 'has-error has-warning';
      add = 'has-success'; 
      name = true;
    }
    else {
      remove = 'has-success has-warning';
      add = 'has-error';
      name = false;
    }
    changeInputState(registerName, remove, add);
    checkToEnableButton();
  });
  
  registerEmail.on('input', function(e, t) {
    var remove, add = "";
    if(registerEmail.val() != ""){
      remove = 'has-error has-warning';
      add = 'has-success'; 
      state= true;
    }
    else {
      remove = 'has-success has-warning';
      add = 'has-error';
      state = false;
    }
    changeInputState(registerEmail, remove, add);
    checkToEnableButton();
  });

  function inputPasswordFieldStates(){
    var classesToRemove, classToAdd = "";
    if(registerPassword.val() != confirmPassword.val())  {
      classesToRemove = 'has-success has-warning';
      classToAdd = 'has-error';
      password= false;
      confirm = false;
    } else {
      classesToRemove = 'has-error has-warning';
      classToAdd= 'has-success';
      if(registerPassword.val() != "") {
        confirm = true;
        password = true;
      }
    }
    
    changeInputState(registerPassword, classesToRemove, classToAdd);
    changeInputState(confirmPassword, classesToRemove, classToAdd);
  }

  function changeInputState(inputElem, remove, add) {
    var group = inputElem.closest(".form-group");
    group.removeClass(remove);
    group.addClass(add);
  }

  function checkToEnableButton() {
    if (name && state && confirm && password) {
      console.log("Enabling the register button ");
      $("#registerbtn").prop("disabled", false);
    }
    else {
      console.log("Disabling the register button ");
      $("#registerbtn").prop("disabled", true);
    }
  }
})


