(function() {
  var Patients, conditions;

  conditions = ["condition1", "condition2", "condition3"];

  Patients = (function() {
    var patients;

    patients = [];

    function Patients() {}

    Patients.prototype.conditionList = function(patient) {
      conditions = patient.conditions;
      if (conditions && conditions.length > 0) {
        return conditions.reduce(function(cond1, cond2) {
          return "" + cond1 + ", " + cond2;
        });
      } else {
        return "";
      }
    };

    Patients.prototype.filter = function() {};

    Patients.prototype.remove = function(patientID) {
      var newPatients;
      newPatients = patients.filter(function(patient) {
        return patient._id !== patientID;
      });
      patients = newPatients;
      return $("#" + patientID).closest("tr").remove();
    };

    Patients.prototype.clearPatientList = function() {
      return $("#active-patients-list").empty();
    };

    Patients.prototype.addToDOM = function(patient) {
      var conditionList, date, id, list, phone;
      console.log("adding the patient to the dom");
      list = $("#active-patients-list");
      conditionList = this.conditionList(patient);
      phone = patient.phone;
      date = patient.entryDate;
      id = patient._id;
      list.append("<tr> \n  <td> Phone: " + phone + " </td>\n  <td> Entry Date: " + date + " </td>\n  <td> " + conditionList + "</td>\n  <td><button type=\"button\"  aria-label=\"Close\" id=\"" + id + "\"><span aria-hidden=\"true\">&times;</span></button></td>\n</tr>");
      return this.appendRemovePatientHandler($("#" + id));
    };

    Patients.prototype.add = function(patient) {
      return patients.push(patient);
    };

    Patients.prototype.appendRemovePatientHandler = function(el) {
      console.log("appending click handler");
      return el.on("click", function() {
        var patientId;
        patientId = el.closest('tr').attr('id');
        return socket.emit('patient nonactive', el.attr('id'));
      });
    };

    Patients.prototype.getAllPatients = function(callback) {
      var promise, that;
      that = this;
      promise = $.ajax({
        url: '/patients/all',
        success: function(patients) {
          var patient, _i, _len, _results;
          console.log("success retrieving patients");
          _results = [];
          for (_i = 0, _len = patients.length; _i < _len; _i++) {
            patient = patients[_i];
            that.add(patient);
            _results.push(that.addToDOM(patient));
          }
          return _results;
        }
      });
      return promise.fail(function(err) {
        alert("there was an error retrieving patients");
        console.log(err);
        return console.log("make this into an alert banner");
      });
    };

    Patients.prototype.getActivePatients = function(callback) {
      var promise, that;
      that = this;
      promise = $.ajax({
        url: '/patients/active',
        success: function(patients) {
          console.log("success retrieving active patients");
          return patients.forEach(function(patient) {
            that.add(patient);
            return that.addToDOM(patient);
          });
        }
      });
      return promise.fail(function(err) {
        alert("there was an error retrieving active patients");
        console.log(err);
        return console.log("make this into an alert banner");
      });
    };

    return Patients;

  })();

  window.Patients = new Patients();

}).call(this);
