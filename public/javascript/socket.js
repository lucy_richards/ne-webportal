(function() {
  window.socket = io();

  socket.on('new patient', function(patient) {
    console.log('New patient!');
    Patients.add(patient);
    return Patients.addToDOM(patient);
  });

  socket.on('patient nonactive', function(patientID) {
    console.log('Removing patient from the DOM');
    return Patients.remove(patientID);
  });

}).call(this);
