# Noora Health's Online Web Portal for Nurse Educators #

Add patients to the Noora Health curriculum, upload files, and view active patients.

### Set Up ###
* Clone repo
* $npm install
* $npm start
* To test: $mocha test