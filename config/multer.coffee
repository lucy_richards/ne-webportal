module.exports = {
  dest: "./public/uploads/"
  limits: {
    fileSize: 1000000
    
  }
  rename: (fieldname, filename) ->
    console.log "renaming the file"
    return filename+new Date().getTime
  
  onError: (error, next) ->
    console.log "There was an error processing the file:#{error}" 
    next(error)
}
