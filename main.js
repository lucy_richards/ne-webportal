var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var coffeescript = require('coffee-script').register();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passportLocalMongoose = require('passport-local-mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var session = require('express-session');
var hbs = require('express-handlebars');
var multer = require('multer');

//MODELS
var NurseEducator = require('./models/NurseEducatorModel');

//ROUTES
var register = require('./routes/register');
var login = require('./routes/login');
var index = require('./routes/index');
var logout = require('./routes/logout');
var patients = require('./routes/patients');

var app = express();
//Mongoose config
mongoose.connect(process.env.MONGO_URL 
                 || 'mongodb://lucyanne:artichokes@lighthouse.0.mongolayer.com:10104/production');

// view engine setup
app.engine('hbs', hbs({
  defaultLayout: 'layout',
  extname: '.hbs',
  partialsDir: __dirname + '/views/partials'
}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(multer(require("./config/multer")));
app.use(cookieParser());
app.use(session({
  resave: true, 
  saveUninitialized: true,
  secret: "Noora Health nurse educators",
  maxAge: 6000
}));
app.use(flash());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/bower_components',  express.static(__dirname + '/bower_components'));
app.use(passport.initialize());
app.use(passport.session());

//PASSPORT CONFIG
passport.use(NurseEducator.createStrategy());

passport.serializeUser(NurseEducator.serializeUser());
passport.deserializeUser(NurseEducator.deserializeUser());

app.use('/register', register);
app.use('/login', login);

app.use( function (req, res, next) {
  if(req.user)
    next();
  else {
    res.redirect('/login');
  }
});

app.use('/', index);
app.use('/logout', logout);
app.use('/patients', patients);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
