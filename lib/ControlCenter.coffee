
Patient = require '../models/PatientModel'

class ControlCenter

  #empty constructor
  constructor: () ->

  addPatient: (options, callback)->
    patient = new Patient options
    patient.save callback

  getActivePatients: (callback) ->
    console.log "control center is getting patients"
    Patient.find({}, callback)
    
module.exports = new ControlCenter()
