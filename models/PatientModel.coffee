mongoose = require 'mongoose'
Schema = mongoose.Schema

patientSchema =
  nurse: String
  phone: String
  entryDate: Date
  conditions: [String]
  tags: [String]

patient = new Schema patientSchema

module.exports = mongoose.model 'patients' , patient

