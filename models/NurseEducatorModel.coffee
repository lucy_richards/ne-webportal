mongoose = require 'mongoose'
Schema = mongoose.Schema
passportLocalMongoose = require 'passport-local-mongoose'

neSchema =
  name: String
  photo: String
  indianState: String

NurseEducatorSchema = new Schema neSchema

options =
  usernameField : 'email'
  incorrectUsernameError: '%s not found, please register or use a different email.'
  incorrectPassswordError: 'Incorrect password'
  userExistsError: '%s is already registered'

NurseEducatorSchema.plugin passportLocalMongoose, options

module.exports = mongoose.model 'nurse_educators' , NurseEducatorSchema
